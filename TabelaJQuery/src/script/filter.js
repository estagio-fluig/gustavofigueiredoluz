function initializeSearch() {
    search.on('input', () => {
        const text = search.val();
        if (text != '') {
            users.forEach(function(user){
                const id = user.id;
                data = [user.firstName, user.lastName, user.email];
                if (RegExp(text, 'i').test(data)) {
                    unfilterUser(id);
                } else {
                    filterUser(id);
                }
            })
        } else {
            filteredUsers.forEach(function(user){
                user.filter = false;
            })
        }
        showRows(1);
    })
}
